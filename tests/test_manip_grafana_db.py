from .. import manip_grafana_db
from nose import tools


def test_insert_dashboard_into_grafana_db():
    import mock
    test_cursor = mock.Mock(execute=mock.Mock())
    test_conn = mock.Mock(cursor=mock.Mock(return_value=test_cursor))
    test_conn.insert_id = mock.Mock(return_value=42)
    test_obj = {"Good Json": "Bad content"}
    manip_grafana_db.initialize()
    manip_grafana_db.SQL_CONNECTION = test_cursor
    tools.assert_raises(manip_grafana_db.GrafanaDataManipulationException,
                        manip_grafana_db.insert_dashboard_into_grafana_db, test_obj)


# Test values
