======
 Plan
======


Notes
=====

Copy grafana files from mondemand server on quorra machines just to
get grafana on those machines.

* Don't really want grafana, just graphite

* Which machines, quorra-agent?

  * yes, but excluding quorra-agent-nginx which are the munin boxes
  * One machine in particular is having issues when graphite is
    installed on the machine, prod-cdh-grid-monitor-xv-01. Apparently
    the graphite.conf overrides DocumentRoot which breaks the webserver
    systems that monitor the grid.

* Files:

  * from quorra-realtime

    * files/opt/bin/quorra-make-links

      * Needs an update to remove _ph tag from metric directory

    * files/etc/cron.d/quorra-make-links.global

      * Needs to have dead symlink and empty directory removal re-enabled.

  * files/etc/httpd/conf.d/graphite.conf
  * scripts/fixfiles (sections that replace placeholders in graphite.conf)
  * scripts/preinstall (sections that replace placeholders in graphite.conf)

Need to pull the graphs data out of the existing .xaap files, which
tells how to generate the graphs.

Will the grafana links really need to be generated?

* On quorra-agent yes.

quorra-main


* No need for mondemand-server

  * just add to quorra-main

    * CARBON=python-carbon-0.9.13-3
    * WHISPER=python-whisper-0.9.13-3
    * GRAPHITE=python-graphite-web-0.10.0_alpha-4
    * RRDTOOL=rrdtool-1.4.8-7
    * RRDTOOL_PYTHON=rrdtool-python-1.4.8-7

  * NOT quorra-agent, but quorra-main

  * fixfiles

    * /etc/httpd/conf.d/graphite.conf
    * restart httpd

Need to run links script on quorra-agent

Interesting roles

* quorra
* quorra-agent
* quorra-agent-nginx
* quorra-main

Possilby interesting

* quorra-realtime

Directories not in make-links
=============================

* /var/lib/quorra/

  * What makes /var/lib/quorra?

    * /etc/xinitd.d/rrdsrv, rrdsrv as a tool, in quorra-main role

mondemand creates rrdg directory

there are differences in naming conventions between quorra-realtime
(realtime has '..' before platform-hash), which is what make-links was
written for vs quorra-agent (quorra-main), which will need to be
addressed.

Might just have to run Ed's make links script and it will work.

Talk with A.Molinaro
====================

Not quorra-agent after all, quorra-main. Quorra agent just sends data
to quorra-main when it is asked. rrds are on quorra-main so grafana
needs to go on quorra-main.

Running of quorra-make-links
============================

* must be /usr/local/bin/perl quorra-make-links
* adds '_ph' to the end of each metric directory?

Other http services running on quorra-main
==========================================

Turns out this is an issue due to the way the graphite.conf and
/opt/graphite/webapp/graphite/local_settings.py were configured. Once
they were reconfigured this was not an issue.


Testing new configs
===================

Copy to c6 dev machine, run there, install grafana.


Quorra rrd filenames and program names
======================================

A metric with the program name as part of the metric name must be in a directory that is named the same way as the program name, otherwise incorrect link names will be created in mondemand/data/rrdg.

::

   mondemand-ox_broker_delivery-rt-request.fill.nofill.noad.mktineligible-counter-cluster=..-ph=ff5ef4de-6324-4f67-ab6f-c060e1018852-SUM.rrd
   for example, the above should be in
   /var/lib/quorra/mondemand/ox_broker_delivery-rt/mondemand-ox_broker_delivery-rt-request.fill.nofill.noad.mktineligible-counter-cluster=..-ph=ff5ef4de-6324-4f67-ab6f-c060e1018852-SUM.rrd

2015-07-07
==========

quorra-main is ready to merge.
* 2015-07-08 merged

* ingest xaap file (just yaml)
* find graph region find metrics

  * find matching metrics in mondemand/grafana metrics as those listed
    in xaap graph region.

    * found, but they have customer name as part of the directory
      name. May or may not be a problem

  * get matching metric into grafana json template.

* caaa-g22 not creating links?

  * lock file stuck, but removed lock file, didn't rectify situation.
  * quorra links not being created correctly

Links not being created correctly
=================================

* copy over problematic directories to c6 test env, such as

  * ntd_ph_ox3
  * run quorra-make-links

    * Doesn't have _ph at end but still has incorrect _gauge

* quorra rrd's are generated differently that mondemand rrds

  * Quorra: program_name/program_name-metric_name-host_name-ctx=ctxv-file.rrd
  * Mondemand: program_name/metric_name/program_name-metric_name-host_name-ctx=ctxv.rrd
  * Still needs metric type (counter|gauge)


Problems on 2015-07-18 run
==========================

* quorra on caaa-g22 hasn't run since 2013??

  * It has, but is being run as root, cant hard link to root files
  * quorra-make-links on realtime runs as apache and creates links fine

* hard links cannot be created across devices on caaa-g22
* xvaaa-xx-02 doesn't have mondemand, how are quorra links supposed to get into grafana?

  * Install graphite, and have quorra-main or graphite create the
  /var/lib/mondemand/data/rrdg directory structure as a temporary
  quorra hack.

* Resulting quorra graphs in grafana are too large, E.Silva updated
  the db to use longtext(16GB) instead of text(64K)

Problems Saving 2015-08-21
==========================

* tags need to be a list, was passing a string.


Collecting xaap files
=====================

::

   find ../roles/quorra* -name "*.xaap" -exec cp {} collected_xaap \;


100 or so files not converted due to disallowed characters starting tokens
==========================================================================

* Stripped trailing whitespace
* Assuming collected_xaap directory from **Collecting xaap files** above:

  ::

     for xaap_file in `ls collected_xaap`; do
       perl -pi -e 's/vertical-label : %/vertical-label : _%/g' collected_xaap/$xaap_file
     done

Missing tags on quorra dashboards
=================================

Need to update the dashboard_tag table with the id of the inserted json data

Datapoints not available in quorra
==================================

Missing metrics? or really ancient dashboards?

Test metric to look for:

* riak.prod.xv.quorra.cpu-idle.derive.MIN
* ox3-broker.quorra.lbstat_frequency-remote_bin.counter.SUM

Graphs not displayed after importing dashboard
==============================================

* Graphs within panels cannot have the same id (cannot both be 'null', must start at 1)

Metric path rewrite 2015-10-05
==============================

* <program_id> - ssrtb

  * <collection_type> - agg

    * <metric_name> - total_req

      * <context_name_1> - acctid

        * <context_value_1> - 537073295

          * cluster (aka colo) - cluster

            * <cluster_locatoin> - ca

              * host - host machine

                * <host_value> - all

                  * <metric_type> - sum


Possible problem metrics
========================

Failed to convert collected_xaap/xv-ssrtb.xaap: contains old metric "ssrtb.agg.total_req.acctid.537110398.cluster.xv.host.all.sum.value". (only has lc)

Failed to convert collected_xaap/xv-ssrtb-error.xaap: contains old metric "ssrtb.agg.error_connect.acctid.537110398.cluster.xv.host.all.sum.value". (only has lc)

Failed to convert collected_xaap/xv-ssrtb-health10.xaap: contains old metric "ssrtb.agg.total_req.acctid.537073300.cluster.xv.host.all.sum.value". (only has ca lc)

AVG to count
============

need to convert all AVG metrics to just count as avg isn't coming up
on aggregate.

manip datasources
=================

`database` is a keyword, but used as a field in the data_source
table.. needs to be quoted with backticks(`) in sql statements.

Because of above issue, REALLY should be using an ORM - sqlalchemy
==================================================================
`Oldish but good tutorial <http://www.blog.pythonlibrary.org/2010/09/10/sqlalchemy-connecting-to-pre-existing-databases>/`_

Select/Insert/Delete to sql alchemy
===================================
`flask page <http://www.blog.pythonlibrary.org/2010/09/10/sqlalchemy-connecting-to-pre-existing-databases/>`_

Datasources
===========

Realtime needs to be dealt with

CA has screwed up metric paths, causing bad results for validate metrics

Dashboards with issues
======================

* Rack Switches (XV-Vertica)

  * bad in production dashboard too

* Vertica MSTR ER (CA)

* MCP Builds

* Pagerduty Nonactionable Alerts

* Grid - QA

* Storage Grid

* Prod Grid

  * not update-able: hadoop.cpu.select metric -- hadoop.md.cpu

* rrdcached stats

  * indicated bad stat is good in both cases
  * was already in good state, just used * instead of md

Dashboards that freeze
======================

prod-grid
ca-ssrtb-notifications
xv-ssrtb-notifications
xa-ssrtb-notifications
xf-ssrtb-notifications
lc-ssrtb-notifications
SSRTB
SSRTB-Health-Top-10
SSRTB-Health-Top-10-Dup-520
SSRTB-Health-Top-10-Dup-87
SSRTB-Health-Top-10-Dup-714
SSRTB-Health-Top-10-Dup-95

* froze because mysql connection was lost, just reconnect every time
  we attempt to make a cursor

  * this wont be an issue if the script uses an orm (sqlalchemy)

Plan
====

Need to create grafana python api, pull out sql interactions into
different lib (and orm that lib).

dashboards converted from xaap files aren't using references correctly
======================================================================
